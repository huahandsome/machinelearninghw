#ifndef __HMM_
#define __HMM__

#define MAX_ITERATOR 10000
#define SEED         2000

void forward_algo(double (*p_alpha)[N], 
                  double (*p_scale_alpha)[N],
                  const int* observation_sequence, 
                  double (*const p_matrix_A)[N], 
                  double (*const p_matrix_B)[M], 
                  const double* p_pi, 
                  double* p_scale,
                  long T);

void backward_algo(double (*p_beta)[N], 
                   double (*p_scale_beta)[N],
                   const int* observation_sequence,
                   double (*const p_matrix_A)[N], 
                   double (*const p_matrix_B)[M], 
                   const double* p_pi, 
                   const double* p_scale,
                   long T);

void compute_di_gamma_and_gamma(double (*p_gamma)[N], 
                                double (*p_di_gamma)[N][N],
                                double (*const p_alpha)[N], 
                                double (*const p_beta)[N], 
                                double (*const p_matrix_A)[N], 
                                double (*const p_matrix_B)[M], 
                                const int* observation_sequence,
                                long T);

void reestimate_pi(double* p_pi, double (*const p_gamma)[N]);

void reestimate_matrix_A(double (*p_matrix_A)[N], double (*const p_gamma)[N], double (*const p_di_gamma)[N][N], long T);

void reestimate_matrix_B(double (*p_matrix_B)[M], double (*const p_gamma)[N], const int* observation_sequence, long T);

double compute_probability(const double* p_scale, long T);

void init_matrix_A(double (*p_matrix_A)[N]);

void init_matrix_B(double (*p_matrix_B)[M]);

void init_matrix_Pi(double *p_matrix_Pi);
#endif
