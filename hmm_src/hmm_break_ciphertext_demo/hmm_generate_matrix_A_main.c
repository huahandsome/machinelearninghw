#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "hmm.h"
#include "hmm_log.h"
#include "hmm_data.h"

int main(){

    double matrix_A[N][N];
    memset(matrix_A, 0.0, sizeof(double)*N*N);
    init_matrix_A(matrix_A);

    long T = get_observation_length("./sample_data.txt");
    
    char* p_observation_char  = (char*)malloc(sizeof(char)*T);

    get_observation_char(p_observation_char, "./sample_data.txt");
    verify_data(p_observation_char, T);
     
    remove_space(p_observation_char, T);
    
    //update T
    T = get_string_length(p_observation_char);
    printf("data length :%ld\n", T);
    verify_data_with_alphabet(p_observation_char, T);
    printf("%s\n", p_observation_char);    
    
    compute_transition_probability(matrix_A, p_observation_char, T);
    print_matrix_A_array(matrix_A);
   
    return 0;
} 
