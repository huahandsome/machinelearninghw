#include <stdio.h>
#include <stdlib.h>
#include <math.h>   //log function
#include "hmm.h"

// global var 
//double p_scale[T] = {0.0, 0.0, 0.0};

// assume observation_sequence combined with integer
// alpha[][] = non_scale_alpha
// scale_alpha[][] = scale_alpha
/*
 * In order to efficient debug, introduce scale_xxx and xxx
 * */

void forward_algo(double (*p_alpha)[N], double (*p_scale_alpha)[N], const int* observation_sequence, 
                  double (*const p_matrix_A)[N], double (*const p_matrix_B)[M], const double* p_pi, double* p_scale, long T){

    int hidden_state = 0;
    int observation_value = 0;
    long t = 0;

    double scale = 0.0;

    // compute alpha_0(i) 
    for( hidden_state = 0; hidden_state < N; hidden_state++ ){
        observation_value = observation_sequence[t];

        p_alpha[t][hidden_state] = p_pi[hidden_state] * p_matrix_B[hidden_state][observation_value]; 
        scale += p_alpha[t][hidden_state];
    }
     
    // scale the alpha_0(i)
    scale = 1/scale;
    p_scale[t] = scale;
    for( hidden_state = 0; hidden_state < N; hidden_state++ ){
        p_scale_alpha[t][hidden_state] = scale * p_alpha[t][hidden_state];    
    }

    // compute alpha_t(i), t >= 1
    int curr_state = 0;
    int pre_state = 0;
    
    for( t = 1; t < T; t++ ){
        observation_value = observation_sequence[t];

        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_alpha[t][curr_state] = 0;    //initial

            for( pre_state = 0; pre_state < N; pre_state++ ){
                p_alpha[t][curr_state] += p_alpha[t-1][pre_state] * p_matrix_A[pre_state][curr_state];
            }

            p_alpha[t][curr_state] = p_alpha[t][curr_state] * p_matrix_B[curr_state][observation_value];
        }
        
        // prepare for scale_alpha_t(i)
        scale = 0.0;
        
        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_scale_alpha[t][curr_state] = 0;    //initial

            for( pre_state = 0; pre_state < N; pre_state++ ){
                p_scale_alpha[t][curr_state] += p_scale_alpha[t-1][pre_state] * p_matrix_A[pre_state][curr_state];
            }

            p_scale_alpha[t][curr_state] = p_scale_alpha[t][curr_state] * p_matrix_B[curr_state][observation_value];

            scale += p_scale_alpha[t][curr_state];
        }

        // scale alpha_t(i)
        scale = 1/scale;
        p_scale[t] = scale;
        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_scale_alpha[t][curr_state] = scale * p_scale_alpha[t][curr_state];
        }
    } 
}

void backward_algo(double (*p_beta)[N], double (*p_scale_beta)[N], const int* observation_sequence,
                   double (*const p_matrix_A)[N], double (*const p_matrix_B)[M], const double* p_pi, const double* p_scale, long T){

    int curr_state = 0;
    int next_state = 0;
    long t = T-1;
    int observation_value; 

    // compute beta[T-1][i], scale_beta[T-1][i]
    for( curr_state = 0; curr_state < N; curr_state++ ){
        p_beta[t][curr_state] = 1;
        p_scale_beta[t][curr_state] = p_scale[T-1];
    }

    // compute beta[t][i], (0 <= t <= T-2)
    for ( t = T-2; t >= 0; t-- ){
        observation_value = observation_sequence[t+1];
        
        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_beta[t][curr_state] = 0;    //initial
            
            for( next_state = 0; next_state < N; next_state++ ){
                p_beta[t][curr_state] += p_matrix_A[curr_state][next_state] * p_matrix_B[next_state][observation_value] * p_beta[t+1][next_state];
            }

            // scale beta
            //p_scale_beta[t][curr_state] = p_scale[t] * p_beta[t][curr_state];
        }
    }

    // compute scale_beta[t][i], (0 <= t <= T-2)
    for ( t = T-2; t >= 0; t-- ){
        observation_value = observation_sequence[t+1];
        
        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_scale_beta[t][curr_state] = 0;    //initial
            
            for( next_state = 0; next_state < N; next_state++ ){
                p_scale_beta[t][curr_state] += p_matrix_A[curr_state][next_state] * p_matrix_B[next_state][observation_value] * p_scale_beta[t+1][next_state];
            }

            // scale beta
            p_scale_beta[t][curr_state] = p_scale[t] * p_scale_beta[t][curr_state];
        }
    }
}

void compute_di_gamma_and_gamma(double (*p_gamma)[N], double (*p_di_gamma)[N][N], 
                                double (*const p_alpha)[N], double (*const p_beta)[N], 
                                double (*const p_matrix_A)[N], double (*const p_matrix_B)[M], const int* observation_sequence, long T){

    long t = 0;
    double denom = 0;
    int curr_state = 0;
    int next_state = 0;
    int observation_value;

    for( t = 0; t < T-1; t++ ){
        denom = 0;
        observation_value = observation_sequence[t+1];

        for( curr_state = 0; curr_state < N; curr_state++ ){
            for( next_state = 0; next_state < N; next_state++ ){
                denom += p_alpha[t][curr_state] * p_matrix_A[curr_state][next_state] * p_matrix_B[next_state][observation_value] * p_beta[t+1][next_state];    
            }
        }
        //printf("t = %ld, denom = %f\n", t, denom);
        if( fabs(denom - 0 ) < 0.00000001){
            denom = 1;  
        }

        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_gamma[t][curr_state] = 0;

            for( next_state = 0; next_state < N; next_state++ ){
                p_di_gamma[t][curr_state][next_state] = p_alpha[t][curr_state] * p_matrix_A[curr_state][next_state] * p_matrix_B[next_state][observation_value] * p_beta[t+1][next_state];

                p_di_gamma[t][curr_state][next_state] /= denom;
                p_gamma[t][curr_state] += p_di_gamma[t][curr_state][next_state];
            }
        }
    }

    // Special case for gamma_(T-1)(i)
    denom = 0;
    for( curr_state = 0; curr_state < N; curr_state++ ){
        denom += p_alpha[T-1][curr_state];    
    }

    for( curr_state = 0; curr_state < N; curr_state++ ){
        p_gamma[T-1][curr_state] = p_alpha[T-1][curr_state]/denom;
    }
}

void reestimate_pi(double* p_pi, double (*const p_gamma)[N]){

    for( int hidden_state = 0; hidden_state < N; hidden_state++ ){
        p_pi[hidden_state] = p_gamma[0][hidden_state];    
    }
}

void reestimate_matrix_A(double (*p_matrix_A)[N], double (*const p_gamma)[N], double (*const p_di_gamma)[N][N], long T){

    for( int curr_state = 0; curr_state < N; curr_state++ ){
        for( int next_state = 0; next_state < N; next_state++ ){
            double numer = 0;
            double denom = 0;

            for( long t = 0; t < T-1; t++ ){
                numer += p_di_gamma[t][curr_state][next_state];
                denom += p_gamma[t][curr_state];
            }
            
            // transition probability
            p_matrix_A[curr_state][next_state] = numer/denom;
        }    
    }
}

void reestimate_matrix_B(double (*p_matrix_B)[M], double (*const p_gamma)[N], const int* observation_sequence, long T){

    for(int hidden_state = 0; hidden_state < N; hidden_state++ ){
        for( int observation_value = 0; observation_value < M; observation_value++){
            double numer = 0;
            double denom = 0;

            for( long t = 0; t < T; t++){
                int sequence_observation_value = observation_sequence[t];
                
                if( observation_value == sequence_observation_value ){
                    numer += p_gamma[t][hidden_state]; 
                }

                denom += p_gamma[t][hidden_state];
            }

            p_matrix_B[hidden_state][observation_value] = numer/denom;
        }    
    }    
}

double compute_probability(const double* p_scale, long T){
    double logProb = 0;

    for( long t = 0; t < T; t++ ){

        if( fabs(p_scale[t] - 0) < 0.00000000001){
            printf("Warning: p_scale[%ld] = %f, ignore this element\n", t, p_scale[t]);
            continue;
        }

        logProb += log(p_scale[t]);
    }

    return (-logProb);
}

void init_matrix_A(double (*p_matrix_A)[N]){

    for(int i = 0; i < N; i++){
        double row_sum = 0.0;
        for(int j = 0; j < N; j++){
            if( j == N-1){
                p_matrix_A[i][j] = 1 - row_sum;       //row stochastic 
            }else{
                int random_num = (rand() % 10 + 1)+ N*10; //[N-1, N+1] 
                p_matrix_A[i][j] = 10.0/(double)random_num; 
                row_sum += p_matrix_A[i][j];
            }
        }    
    }
}

void init_matrix_B(double (*p_matrix_B)[M]){

    for(int i = 0; i < N; i++){
        double row_sum = 0.0;
        for(int j = 0; j < M; j++){
            if( j == M-1){
               p_matrix_B[i][j] = 1 - row_sum;        //row stochastic 
            }else{
                //int random_num = (rand() % 10 + 1)+ M*10; //[M-1, M+1] 
                int random_num = (double)rand()/RAND_MAX * (10) + 265; //[M-1, M+1] 
                p_matrix_B[i][j] = 10.0/(double)random_num; 
                row_sum += p_matrix_B[i][j];
            }
        }    
    }
}

void init_matrix_Pi(double *p_matrix_Pi){
 
    double row_sum = 0.0;
    for(int i = 0; i < N; i++){
        if( i == N-1){
            p_matrix_Pi[i] =  1 - row_sum;        //row stochastic 
        }else{
            int random_num = (rand() % 10 + 1)+ N*10; //[N-1, N+1] 
            p_matrix_Pi[i] = 10.0/(double)random_num; 
            row_sum += p_matrix_Pi[i];
        }
    }
}
