#include <stdio.h>
#include <stdlib.h>
#include "hmm.h"

void print_matrix_A(double (*const p_matrix_A)[N]){
    printf("\n---   Matrix A   ---\n");
    
    for( int state_row = 0; state_row < N; state_row++ ){
        for( int state_col = 0; state_col < N; state_col++ ){
            printf("%10f ", p_matrix_A[state_row][state_col]); 
        
            if( (state_col+1)%8 == 0 ){ //each row display 8 elements max
                printf("\n");
            }
        }
        printf("\n");
    }
}

void print_matrix_A_array(double (*const p_matrix_A)[N]){
    printf("\n---   Matrix A   ---\n");
    
    for( int state_row = 0; state_row < N; state_row++ ){
        printf("{\n");
        for( int state_col = 0; state_col < N; state_col++ ){
            printf("%10f ,", p_matrix_A[state_row][state_col]); 
        
            if( (state_col+1)%8 == 0 ){ //each row display 8 elements max
                printf("\n");
            }
        }
        printf("},\n");
    }
}

void print_matrix_B(double (*const p_matrix_B)[M]){
    printf("\n---   Matrix B   ---\n");

    for( int state_row = 0; state_row < N; state_row++ ){
        for( int state_col = 0; state_col < M; state_col++ ){
            printf("%10f ", p_matrix_B[state_row][state_col]); 
            
            if( (state_col+1)%8 == 0 ){ //each row display 8 elements max
                printf("\n");
            }
        }
        printf("\n");
    }
}

void print_matrix_B_Transpose(double (*const p_matrix_B)[M]){
    printf("\n---   Transpose Matrix B   ---\n");
    
    double(*p_transpose_matrix)[N] = malloc(sizeof(double)*M*N);

    for( int state_row = 0; state_row < N; state_row++ ){
        for( int state_col = 0; state_col < M; state_col++ ){
            p_transpose_matrix[state_col][state_row] = p_matrix_B[state_row][state_col];
    //        printf("%10f ", p_matrix_B[state_col][state_row]); 
        }
    //  printf("\n");
    }

    for( int state_row = 0; state_row < M; state_row++ ){
        for( int state_col = 0; state_col < N; state_col++ ){
            printf("%10f ", p_transpose_matrix[state_row][state_col]); 
            
            if( (state_col+1)%8 == 0 ){ //each row display 8 elements max
                printf("\n");
            }
        }
        printf("\n");
    }

    free(p_transpose_matrix);
}


void print_matrix_Pi(const double *p_matrix_Pi){
    printf("\n---   Matrix PI   ---\n");
    
    for( int state = 0; state < N; state++){
        printf("%10f ", p_matrix_Pi[state]); 

        if( (state + 1)%8 == 0){
            printf("\n");
        }
    }
    printf("\n");
}

#ifdef __DEBUG__

void print_alpha(double (*const alpha)[N], int scale, long T){
    printf("\n");

    if(scale){
         for( long row = 0; row < T; row++ ){
            for( int col = 0; col < N; col++ ){
                printf("scale_alpha_%ld(%d) = %10e ", row, col, alpha[row][col]); 
            }
            printf("\n");
        }       
    }else{
        for( long row = 0; row < T; row++ ){
            for( int col = 0; col < N; col++ ){
                printf("alpha_%ld(%d) = %10e ", row, col, alpha[row][col]); 
            }
            printf("\n");
        }
    }
}

void print_beta(double (*const beta)[N], int scale, long T){
    printf("\n");
    
    if(scale){
        for( long row = 0; row < T; row++ ){
            for( int col = 0; col < N; col++ ){
                printf("scale_beta_%ld(%d) = %15e ", row, col, beta[row][col]); 
            }
            printf("\n");
        }
    }else{
        for( long row = 0; row < T; row++ ){
            for( int col = 0; col < N; col++ ){
                printf("beta_%ld(%d) = %15e ", row, col, beta[row][col]); 
            }
            printf("\n");
        }

    }
}


void print_scale(const double *p_scale, long T){
    printf("\n---   Scale   ---\n");
    
    for( long t = 0; t < T; t++){
        printf("%10f ", p_scale[t]); 

        if((t+1)%10 == 0)
            printf("\n");
    }
    printf("\n");
}

void print_gamma(double (*const p_gamma)[N], long T){
    printf("\n---   Gamma   ---\n");
    
    for( long t = 0; t < T; t++ ){ 
        for( int state = 0; state < N; state++)
            printf("gamma_(%ld)[%d]: %10f ", t, state, p_gamma[t][state]); 
        printf("\n");
    }

    printf("\n");
}

void print_di_gamma(double(*const p_di_gamma)[N][N], long T){
    printf("\n---   Di_gamma   ---\n");
    
    for( long t = 0; t < T; t++){
        for( int state_row = 0; state_row < N; state_row++ ){
            for ( int state_col = 0; state_col < N; state_col++ ){
                printf("di_gamma_(%ld)[%d][%d]: %10f ", t, state_row, state_col, p_di_gamma[t][state_row][state_col]);
            }
            printf("\n");
        }
        printf("\n");
    }

    printf("\n");
}
#endif
