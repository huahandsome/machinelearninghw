#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include "hmm_data.h"
#include "hmm_log.h"

void get_observation_char(char* buff, const char* filename){
   int fd = -1;
   ssize_t bytes_read = -1;

   fd = open(filename, O_RDONLY);
   if( -1 == fd ){
       perror("Open file failed\n");
       return;
   }

   while( (bytes_read = read(fd, buff, MAX_FILE_SIZE)) > 0){
   }

   close(fd);
}

void verify_data(char* data_in, long len){
    for( long idx = 0; idx < len; idx++ ){
        if (data_in[idx] != ' ' && (data_in[idx] < 'a' || data_in[idx] > 'z')){
            printf("invalid char, idx:%ld, %x\n", idx, data_in[idx]);
            data_in[idx] = ' ';
        }
    }
}

void verify_data_with_alphabet(const char* data_in, long len){
     for( long idx = 0; idx < len; idx++ ){
        if (data_in[idx] < 'a' || data_in[idx] > 'z'){
            printf("invalid char, idx:%ld, %x\n", idx, data_in[idx]);
        }
    }
  
}

long get_observation_length(const char* filename){
   int fd = -1;
   struct stat file_info;

   fd = open(filename, O_RDONLY);
   if( -1 == fd ){
       perror("Open file failed\n");
       return 1;
   }

   fstat(fd, &file_info);
   
   close(fd);
   return file_info.st_size;
}

long get_string_length(const char* str){
   long len = 0;

   while(str[len++] != '\0');

   return len;
}

void map_observation_value(const char* src, int* dst, long len){
//    int delta = 'a' - '0';   //used for char to int

    for(long idx = 0; idx < len; idx++){
        if( src[idx] >= 'a' && src[idx] <= 'z' ){
            dst[idx] = src[idx] - 'a';    //'a' to 'z' map to 0 ~ 25 respectively 
        }else if( src[idx] == ' ' ){    
            dst[idx] = 26;  //space maps to 26
        }else{
            //perror("invalid character\n");    
            printf("invalid character: %c\n", src[idx]);
        }
    }
}

void remove_space(char* str, long len){
    char* i = str;
    char* j = str;

    for(int idx = 0; idx < len; idx++){
        *i = *j;
        j++;

        if( *i != ' ' ){
            i++;
        }
    }

    *i = '\0';
}

void compute_transition_probability(double (*p_transition_array)[M], const char* p_strings, long str_len){
    
    const char* p_curr = p_strings; 
    const char* p_next = p_strings + 1;

    memset(p_transition_array, 0, N*M*sizeof(double));

    // count 
    for(int idx = 0; idx < str_len; idx++){
        int i = *(p_curr+idx) - 'a';
        int j = *(p_next+idx) - 'a';

        p_transition_array[i][j]++;
    }

    // normalized
    for(int row = 0; row < N; row++){
        for(int col = 0; col < M; col++){
            p_transition_array[row][col] += 5;
        }
    }

    for(int row = 0; row < M; row++){
        int row_sum = 0;

        for(int col = 0; col < M; col++){
            row_sum += p_transition_array[row][col];    
        }
        
        if( row_sum != 0 ){
            for(int col = 0; col < M; col++){
                p_transition_array[row][col] = p_transition_array[row][col]/row_sum; 
            }
        }
    }
    
    //printf("-----------------\n");
    //for(int row = 0; row < N; row++){
    //    double sum = 0.0;
    //    for(int col = 0; col < M; col++){
    //        sum += p_transition_array[row][col];
    //    }
    //    printf(" %f\n", sum);
    //}

    //printf("-----------------\n");
    //for(int row = 0; row < N; row++){
    //    for(int col = 0; col < M; col++){
    //        if ( fabs(p_transition_array[row][col] - 0) < 0.000001){
    //            printf("row: %d, col:%d\n", row, col);    
    //        }
    //    }
    //}
}

void shift_string(char* str, int len, int shift){
    for(int i = 0; i < len; i++){
        if( (str[i] + shift) > 'z' ){
            str[i] = 'a' + (str[i] + shift - 'z') - 1;
        }else{
            str[i] = str[i] + shift;       
        }  
    }
} 

void write_data(const char* filename, const char* buff, size_t count){
   int fd = -1;

   ssize_t size = 0;
   fd = open(filename, O_CREAT | O_WRONLY);
   if( -1 == fd ){
       perror("Open file failed\n");
       return;
   }

   size = write(fd, buff, count);   
   if(size == -1){
       printf("\n\n-----%s----\n\n", strerror(errno));   
   }

   close(fd);
}
