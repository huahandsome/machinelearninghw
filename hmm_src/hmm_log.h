#include "hmm.h"
void print_matrix_A(double (*const matrix_A)[N]);
void print_matrix_A_array(double (*const p_matrix_A)[N]);
void print_matrix_B(double (*const matrix_B)[M]);
void print_matrix_B_Transpose(double (*const matrix_B)[M]);
void print_matrix_Pi(const double *matrix_Pi);

#ifdef __DEBUG__
void print_alpha(double (*const alpha)[N], int isScale, long T);
void print_beta(double (*const beta)[N], int isScale, long T);
void print_scale(const double *p_scale, long T);
void print_gamma(double (*const p_gamma)[N], long T);
void print_di_gamma(double(*const p_di_gamma)[N][N], long T);
#endif
