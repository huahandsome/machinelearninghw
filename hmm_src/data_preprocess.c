#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#define FILE_PATH "/home/huahandsome/Downloads/CMPE-286-Machine_Learning/corpus/sample_data.txt"
#define MAX_FILE_SIZE 4*1024*1024

long get_observation_length();
void get_observation_char(char* buff);
void verify_data(char* data_in, long len);
void put_observation_char(const char* buff, long len);

int main(){

    long len = get_observation_length();
    char* file = (char*)malloc(len);   
    memset(file, 0, len);
    printf("len : %ld\n", len);

    get_observation_char(file);
    printf("string len:%d\n", strlen(file)); 
    verify_data(file, len);
    
    printf("string len1:%d\n", strlen(file)); 
    put_observation_char(file, len);
    return 0;    
}

long get_observation_length(){
   int fd = -1;
   struct stat file_info;

   fd = open(FILE_PATH, O_RDONLY);
   if( -1 == fd ){
       perror("Open file failed\n");
       return 1;
   }

   fstat(fd, &file_info);
   
   close(fd);
   return file_info.st_size;
}

void put_observation_char(const char* buff, long len){
   int fd = -1;

   fd = open("sample_data.txt", O_CREAT|O_WRONLY,S_IRUSR );
   if( -1 == fd ){
       perror("Open file failed\n");
       return;
   }

  // long bytes_write = 0;
   
   for(long idx = 0; idx < len; idx++){
       write(fd, buff+idx, 1);
   }
   
   //if(bytes_write != len){
   //    printf("write length:%ld != len:%ld\n", bytes_write, len);
   //}

   close(fd);
   
}

void get_observation_char(char* buff){
   int fd = -1;
   ssize_t bytes_read = -1;

   fd = open(FILE_PATH, O_RDONLY);
   if( -1 == fd ){
       perror("Open file failed\n");
       return;
   }

   while( (bytes_read = read(fd, buff, MAX_FILE_SIZE)) > 0){
   }

   close(fd);
}

void verify_data(char* data_in, long len){

    for( long idx = 0; idx < len; idx++ ){
        if(data_in[idx] == ' '){
        
        }else if(data_in[idx] >= 'a' && data_in[idx] <= 'z'){
        
        }else if(data_in[idx] >= 'A' && data_in[idx] <= 'Z'){
            data_in[idx] = data_in[idx] + ('a' - 'A');    
        }else{
            printf("invalid char, idx:%ld, %x\n", idx, data_in[idx]);
            data_in[idx] = ' ';
        }
    }
}
