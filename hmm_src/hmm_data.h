#define MAX_FILE_SIZE   16*1024*1024 

void get_observation_char(char* buff, const char* filename);
long get_observation_length(const char* filename);
long get_string_length(const char* str);
void map_observation_value(const char* src, int* dst, long len);
void verify_data(char* data_in, long len);
void verify_data_with_alphabet(const char* data_in, long len);
void remove_space(char* str, long len);
void compute_transition_probability(double (*p_transition_array)[26], const char* p_strings, long str_len);
void shift_string(char* str, int len, int shift);
void write_data(const char* filename, const char* buff, size_t count);
