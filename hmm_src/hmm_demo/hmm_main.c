#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "hmm.h"
#include "hmm_log.h"
#include "hmm_data.h"

//extern double G_Scale[T];

int main(){
    clock_t begin = clock();
    srand(SEED);
#if (N==2)&&(M==27)
#elif (N==3)&&(M==27)
#elif (N==4)&&(M==27)
#elif (N==26)&&(M==27)
#endif
   
    double matrix_A[N][N];
    memset(matrix_A, 0.0, sizeof(double)*N*N);
    init_matrix_A(matrix_A);
    //print_matrix_A(matrix_A);

    double matrix_B[N][M];
    memset(matrix_B, 0.0, sizeof(double)*M*N);
    init_matrix_B(matrix_B);
    //print_matrix_B(matrix_B);

    double matrix_Pi[N];
    memset(matrix_Pi, 0.0, sizeof(double)*N);
    init_matrix_Pi(matrix_Pi);
    //print_matrix_Pi(matrix_Pi);

    double (*alpha)[N]        = NULL;
    double (*scale_alpha)[N]  = NULL;
    double (*beta)[N]         = NULL;
    double (*scale_beta)[N]   = NULL;
    double (*gamma)[N]        = NULL;
    double(*di_gamma)[N][N]   = NULL;

    int keep = 1;
    double curr_probability;
    double pre_probability = log(0);
    //long T = get_observation_length("./sample_data.txt");
    long T = get_observation_length("./sample_data_50k.txt");
    printf("data length: %ld", T);
    
    if(T > MAX_FILE_SIZE){
        perror("Too large file, will truncate\n");
        T = MAX_FILE_SIZE;
    }

    char* p_observation_char  = (char*)malloc(sizeof(char)*T);
    int* observation_sequence = (int*)malloc(sizeof(int)*T);

    //get_observation_char(p_observation_char, "./sample_data.txt");
    get_observation_char(p_observation_char, "./sample_data_50k.txt");
    verify_data(p_observation_char, T);

    //encrypt_data
    shift_string(p_observation_char, T, 3);

    map_observation_value(p_observation_char, observation_sequence, T);
    
    alpha       = malloc(sizeof(double)*T*N);
    memset(alpha, 0.0, sizeof(alpha)*T*N);

    scale_alpha = malloc(sizeof(double)*T*N); 
    memset(scale_alpha, 0.0, sizeof(scale_alpha)*T*N);

    beta        = malloc(sizeof(double)*T*N); 
    memset(beta, 0.0, sizeof(beta)*T*N);

    scale_beta  = malloc(sizeof(double)*T*N); 
    memset(scale_beta, 0.0, sizeof(scale_beta)*T*N);

    gamma       = malloc(sizeof(double)*T*N); 
    memset(gamma, 0.0, sizeof(gamma)*T*N);

    di_gamma    = malloc(sizeof(double)*T*N*N);
    memset(di_gamma, 0.0, sizeof(di_gamma)*T*N*N);
    
    double *p_scale = (double*)malloc(sizeof(double)*T); 
    memset(p_scale, 0.0, sizeof(p_scale)*T);

    for( int iters = 0; iters < MAX_ITERATOR && keep; iters++ ){
        printf("+++ %d iterate start, ", iters+1);    

        forward_algo(alpha, 
                     scale_alpha,
                     observation_sequence,
                     matrix_A,
                     matrix_B,
                     matrix_Pi,
                     p_scale,
                     T);
    
        backward_algo(beta, 
                      scale_beta,
                      observation_sequence,
                      matrix_A,
                      matrix_B, 
                      matrix_Pi,
                      p_scale,
                      T);
#ifdef __DEBUG__
        print_alpha(alpha, 0, T);  //non scale alpha
        print_alpha(scale_alpha, 1, T);  //scale alpha
        
        print_beta(beta, 0, T);    //non scale beta
        print_beta(scale_beta, 1, T);    //scale beta
        
        print_matrix_A(matrix_A);
        print_matrix_B(matrix_B);
        print_matrix_Pi(matrix_Pi);
        print_scale(p_scale, T);
#endif
        curr_probability = compute_probability(p_scale, T);
        
        printf("probability: %f ", curr_probability);
        if(curr_probability > pre_probability){
            pre_probability = curr_probability;

            compute_di_gamma_and_gamma(gamma, 
                                       di_gamma, 
                                       scale_alpha, 
                                       scale_beta, 
                                       matrix_A,
                                       matrix_B,
                                       observation_sequence,
                                       T);
#ifdef __DEBUG__
            print_gamma(gamma, T);
            print_di_gamma(di_gamma, T);
#endif 

            reestimate_pi(matrix_Pi, gamma);
            reestimate_matrix_A(matrix_A, gamma, di_gamma, T);
            reestimate_matrix_B(matrix_B, gamma, observation_sequence, T);
        }else{
            keep = 0; //end loop    
        } 

        printf(" %d iterate end +++\n", iters+1);    
    }
    
    print_matrix_A(matrix_A);
//    print_matrix_B(matrix_B);
    print_matrix_B_Transpose(matrix_B);
    print_matrix_Pi(matrix_Pi);
    
    clock_t end = clock();
    double time_spent = (double)(end - begin)/CLOCKS_PER_SEC;

    printf("program running: %f sec\n", time_spent);
    
    // free memory
    free(p_observation_char);
    free(observation_sequence);
    free(alpha); 
    free(scale_alpha); 
    free(beta); 
    free(scale_beta);
    free(gamma);
    free(di_gamma);    
    free(p_scale);
    
    return 0;    
}
