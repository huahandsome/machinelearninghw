#include <stdio.h>
#include <stdlib.h>
#include <math.h>   //log function
#include <string.h>   //log function

#define N 2
#define M 3

double matrix_pi[] = {0.6, 0.4};

double matrix_A[2][2] = {
    {0.7, 0.3},
    {0.4, 0.6}
};

double matrix_B[2][3] = {
    {0.1, 0.4, 0.5},
    {0.7, 0.2, 0.1}
};
int observation_sequence[81][4] = {
   {0,0,0,0}, 
   {0,0,0,1}, 
   {0,0,0,2}, 
   {0,0,1,0}, 
   {0,0,1,1}, 
   {0,0,1,2}, 
   {0,0,2,0}, 
   {0,0,2,1}, 
   {0,0,2,2}, 
   {0,1,0,0}, 
   {0,1,0,1}, 
   {0,1,0,2}, 
   {0,1,1,0}, 
   {0,1,1,1}, 
   {0,1,1,2}, 
   {0,1,2,0}, 
   {0,1,2,1}, 
   {0,1,2,2}, 
   {0,2,0,0}, 
   {0,2,0,1}, 
   {0,2,0,2}, 
   {0,2,1,0}, 
   {0,2,1,1}, 
   {0,2,1,2}, 
   {0,2,2,0}, 
   {0,2,2,1}, 
   {0,2,2,2}, 
   {1,0,0,0}, 
   {1,0,0,1}, 
   {1,0,0,2}, 
   {1,0,1,0}, 
   {1,0,1,1}, 
   {1,0,1,2}, 
   {1,0,2,0}, 
   {1,0,2,1}, 
   {1,0,2,2}, 
   {1,1,0,0}, 
   {1,1,0,1}, 
   {1,1,0,2}, 
   {1,1,1,0}, 
   {1,1,1,1}, 
   {1,1,1,2}, 
   {1,1,2,0}, 
   {1,1,2,1}, 
   {1,1,2,2}, 
   {1,2,0,0}, 
   {1,2,0,1}, 
   {1,2,0,2}, 
   {1,2,1,0}, 
   {1,2,1,1}, 
   {1,2,1,2}, 
   {1,2,2,0}, 
   {1,2,2,1}, 
   {1,2,2,2}, 
   {2,0,0,0}, 
   {2,0,0,1}, 
   {2,0,0,2}, 
   {2,0,1,0}, 
   {2,0,1,1}, 
   {2,0,1,2}, 
   {2,0,2,0}, 
   {2,0,2,1}, 
   {2,0,2,2}, 
   {2,1,0,0}, 
   {2,1,0,1}, 
   {2,1,0,2}, 
   {2,1,1,0}, 
   {2,1,1,1}, 
   {2,1,1,2}, 
   {2,1,2,0}, 
   {2,1,2,1}, 
   {2,1,2,2}, 
   {2,2,0,0}, 
   {2,2,0,1}, 
   {2,2,0,2}, 
   {2,2,1,0}, 
   {2,2,1,1}, 
   {2,2,1,2}, 
   {2,2,2,0}, 
   {2,2,2,1}, 
   {2,2,2,2}
};

// assume observation_sequence combined with integer
// alpha[][] = non_scale_alpha
// scale_alpha[][] = scale_alpha
/*
 * In order to efficient debug, introduce scale_xxx and xxx
 * */
void forward_algo(double (*p_alpha)[N], double (*p_scale_alpha)[N], const int* observation_sequence, 
                  double (*const p_matrix_A)[N], double (*const p_matrix_B)[M], const double* p_pi, double* p_scale, long T);

int main(){
    double(*p_alpha)[4][N] = malloc(81*sizeof(double)*4*N);
    memset(p_alpha, 0, 81*sizeof(double)*4*N);

    double(*p_scale_alpha)[4][N] = malloc(81*sizeof(double)*4*N);
    memset(p_scale_alpha, 0, 81*sizeof(double)*4*N);

    double* p_scale = malloc(sizeof(double)*4);
    memset(p_scale, 0, sizeof(double)*4);

    double sum[81] = {0.0};

    for(int i = 0; i < 81; i++){
        forward_algo(p_alpha[i], p_scale_alpha[i], observation_sequence[i],
                     matrix_A, matrix_B, matrix_pi, p_scale, 4);    
    }
    
    for(int i = 0; i < 81; i++){
        printf("\nobservation sequence: ");
        sum[i] = 0;
        for(int j = 0; j < 4; j++){
            printf("%d ", observation_sequence[i][j]);
        }
        printf("\n");

        for(int j = 0; j < 4; j++){
            for(int k = 0; k < N; k++){
                printf("alpha[%d][%d]:%f        ", j, k, p_alpha[i][j][k]);

                if( j==3 ){    //T-1
                    sum[i] += p_alpha[i][j][k];
                }
            }

            printf("\n");
        }
        printf("sum alpha[T-1][i] is: %f ", sum[i]);
        printf("\n");    
    }

    double total = 0.0;
    for(int i = 0; i < 81; i++){
        total += sum[i]; 
    }
    printf("Total is %f\n", total);

    return 0;
}

void forward_algo(double (*p_alpha)[N], double (*p_scale_alpha)[N], const int* observation_sequence, 
                  double (*const p_matrix_A)[N], double (*const p_matrix_B)[M], const double* p_pi, double* p_scale, long T){

    int hidden_state = 0;
    int observation_value = 0;
    long t = 0;

    double scale = 0.0;

    // compute alpha_0(i) 
    for( hidden_state = 0; hidden_state < N; hidden_state++ ){
        observation_value = observation_sequence[t];

        p_alpha[t][hidden_state] = p_pi[hidden_state] * p_matrix_B[hidden_state][observation_value]; 
        scale += p_alpha[t][hidden_state];
    }
     
    // scale the alpha_0(i)
    scale = 1/scale;
    p_scale[t] = scale;
    for( hidden_state = 0; hidden_state < N; hidden_state++ ){
        p_scale_alpha[t][hidden_state] = scale * p_alpha[t][hidden_state];    
    }

    // compute alpha_t(i), t >= 1
    int curr_state = 0;
    int pre_state = 0;
    
    for( t = 1; t < T; t++ ){
        observation_value = observation_sequence[t];

        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_alpha[t][curr_state] = 0;    //initial

            for( pre_state = 0; pre_state < N; pre_state++ ){
                p_alpha[t][curr_state] += p_alpha[t-1][pre_state] * p_matrix_A[pre_state][curr_state];
            }

            p_alpha[t][curr_state] = p_alpha[t][curr_state] * p_matrix_B[curr_state][observation_value];
        }
        
        // prepare for scale_alpha_t(i)
        scale = 0.0;
        
        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_scale_alpha[t][curr_state] = 0;    //initial

            for( pre_state = 0; pre_state < N; pre_state++ ){
                p_scale_alpha[t][curr_state] += p_scale_alpha[t-1][pre_state] * p_matrix_A[pre_state][curr_state];
            }

            p_scale_alpha[t][curr_state] = p_scale_alpha[t][curr_state] * p_matrix_B[curr_state][observation_value];

            scale += p_scale_alpha[t][curr_state];
        }

        // scale alpha_t(i)
        scale = 1/scale;
        p_scale[t] = scale;
        for( curr_state = 0; curr_state < N; curr_state++ ){
            p_scale_alpha[t][curr_state] = scale * p_scale_alpha[t][curr_state];
        }
    } 
}
