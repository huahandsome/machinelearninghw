#include <stdio.h>
#include <dynamic_programming.h>

#define H 0     //hot
#define C 1     //cold

const double matrix_pi[] = {0.6, 0.4};

const double matrix_A[N][N] = {
    {0.7, 0.3},
    {0.4, 0.6}
};

const double matrix_B[N][M] = {
    {0.1, 0.4, 0.5},
    {0.7, 0.2, 0.1}
};

int hidden_state_array[16][4] = {
    {H, H, H, H},
    {H, H, H, C},
    {H, H, C, H},
    {H, H, C, C},
    {H, C, H, H},
    {H, C, H, C},
    {H, C, C, H},
    {H, C, C, C},
    {C, H, H, H},
    {C, H, H, C},
    {C, H, C, H},
    {C, H, C, C},
    {C, C, H, H},
    {C, C, H, C},
    {C, C, C, H},
    {C, C, C, C}
};

//by default, hidden state length is 4, observation value is 4
//double compute_dp(const int* hidden_state, int O_0, int O_1, int O_2, int O_3){
//
//    double dp_value = 
//               matrix_pi[ (hidden_state[0]) ] * matrix_B[ (hidden_state[0]) ][O_0] *
//               matrix_A[ (hidden_state[0]) ][ (hidden_state[1]) ] * matrix_B[ (hidden_state[1]) ][O_1] *
//               matrix_A[ (hidden_state[1]) ][ (hidden_state[2]) ] * matrix_B[ (hidden_state[2]) ][O_2] *
//               matrix_A[ (hidden_state[2]) ][ (hidden_state[3]) ] * matrix_B[ (hidden_state[3]) ][O_3];
//
//    return dp_value;
//}

int main(){
    double prob = 0.0;
    int observation_sequence[4] = {0};

    //generate all possible observation sequences
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            for(int k = 0; k < 3; k++){
                for(int l = 0; l < 3; l++){
    
                    observation_sequence[0] = i;
                    observation_sequence[1] = j;
                    observation_sequence[2] = k;
                    observation_sequence[3] = l;
                    
                    printf("%d,%d,%d,%d: \n", i, j, k, l);
                    printf("hidden state: \n");
                    
                    double observation_prob = 0.0;

                    for( int hidden_state_idx = 0; hidden_state_idx < 16; hidden_state_idx++ ){

                        for( int s = 0; s < 4; s++){
                            char c = 'H';
                            
                            if(hidden_state_array[hidden_state_idx][s]==C){
                                c = 'C';    
                            }

                            printf("%c, ", c);    
                        }   

                        //double dp = compute_dp(hidden_state_array[hidden_state_idx],i,j,k,l);
                        double dp = prob_by_dynamic_program( matrix_pi,    // matrix pi
                                                             matrix_A,     // matrix A
                                                             matrix_B,     // matrix B
                                                             hidden_state_array[hidden_state_idx],       // one of the hidden states
                                                             observation_sequence,                       // one of the observation sequence
                                                             sizeof(observation_sequence)/sizeof(int) ); // observation sequence length

                        printf("DP: %f\n", dp);
                        observation_prob += dp;
                        prob += dp;
                    }
                    printf("observation prob is: %f\n", observation_prob);
                }
                printf("\n");
            }
            printf("\n");
        }
        printf("\n");
    }

    printf("sum all DP probabilities: %f\n", prob);

    return 0;    
}
