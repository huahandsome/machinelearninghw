
#ifndef __DYNAMIC_PROGRAMMING__
#define __DYNAMIC_PROGRAMMING__

#define N 2
#define M 3

double prob_by_dynamic_program(const double* p_pi,                // matrix pi  
                               const double (*p_matrix_A)[N],     // matrix A
                               const double (*p_matrix_B)[M],     // matrix B
                               const int* p_hidden_state,         // one of the hidden states
                               const int* p_observation_sequence, // one of the observation sequence
                               long observation_len);             // observation length

#endif
