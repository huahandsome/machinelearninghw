#include "dynamic_programming.h"

/*
 * obtain probability under certain hidden state, and certain observation sequence
 * e.x., observation: (0, 1, 0, 2) and hidden_state (H,H,H,H), the probability is:
 *
 * prob = prob_by_dynamic_program(pi, A, B, hidden_state, observation, 4);
 *
*/

double prob_by_dynamic_program(const double* p_pi,                // matrix pi  
                               const double (*p_matrix_A)[N],     // matrix A
                               const double (*p_matrix_B)[M],     // matrix B
                               const int* p_hidden_state,         // one of the hidden states
                               const int* p_observation_sequence, // one of the observation sequence
                               long observation_len){             // observation sequence length

    double probability = 0.0;
    int curr_hidden_state, pre_hidden_state;


    for( int t = 0; t < observation_len; t++ ){

        int observation_value = p_observation_sequence[t];
        
        if( t == 0 ){
            curr_hidden_state = p_hidden_state[t];
            pre_hidden_state = curr_hidden_state;
            probability = p_pi[curr_hidden_state] * p_matrix_B[curr_hidden_state][observation_value];    
        }else{
            pre_hidden_state = curr_hidden_state;
            curr_hidden_state = p_hidden_state[t];
            probability *= p_matrix_A[pre_hidden_state][curr_hidden_state] * p_matrix_B[curr_hidden_state][observation_value];   
        }    
    }    

    return probability;
}
